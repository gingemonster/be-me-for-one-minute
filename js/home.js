$(function(){
    L.mapbox.accessToken = 'pk.eyJ1IjoiYmVtZWZvcm9uZW1pbnV0ZSIsImEiOiJtTnZnU3ZFIn0.u7Woh69Vu9f0R9WQv0vzmQ';
    var map = L.mapbox.map('map', 'bemeforoneminute.k9h8k8gc')
        .setView([52.6273,1.299815], 17);

    var featureLayer = L.mapbox.featureLayer()
        .loadURL('mapdata.json')
        .addTo(map);

    L.control.fullscreen().addTo(map);

    // one layer loaded from geojson
    featureLayer.on('ready', function() {
        // featureLayer.getBounds() returns the corners of the furthest-out markers,
        // and map.fitBounds() makes sure that the map contains these.
        map.fitBounds(featureLayer.getBounds());

        // add popups
        featureLayer.eachLayer(function(layer) {

            // here you call `bindPopup` with a string of HTML you create - the feature
            // properties declared above are available under `layer.feature.properties`
            var content = '<h2>' + layer.feature.properties.name + '<\/h2>';
            if(layer.feature.properties.photourl){
                content += '<img src="images/' + layer.feature.properties.cartodb_id + '.jpg" class="popupimage" />';
            }
            if(layer.feature.properties.videourl){
                content += '<video autoplay class="popupimage"><source src="videos/' + layer.feature.properties.cartodb_id + '.mp4"></video>';
            }
            content +=  '<p>' + layer.feature.properties.description + '</p>';
            layer.bindPopup(content);

            layer.setIcon(L.mapbox.marker.icon({
                'marker-color': '#ff8888',
                'marker-size': 'large'
            }));
        });


    });


});